
package system;

public class Ingrediens {
    private static int ing_id_generator = 0;
    int ingID;
    String name;
    int inventoryGrams;
    
    public Ingrediens(String name, int inventoryGrams) {
        ing_id_generator ++;
        this.ingID = ing_id_generator;
        this.name = name;
        this.inventoryGrams = inventoryGrams;
    }

    public int getIngID() {
        return ingID;
    }

    public String getName() {
        return name;
    }

    public int getInventoryGrams() {
        return inventoryGrams;
    }

    public void addGrams(int gramsAdded) {
        inventoryGrams = inventoryGrams + gramsAdded;
    }

}