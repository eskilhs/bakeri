package system;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Recipe {
    private static int rec_id_generator = 0;

    int recipeID;
    String name;
    SimpleDateFormat date;
    String productType;
    private List<Ingrediens> ingrediensList;

    public Recipe(String name, String productType, List<Ingrediens> ingrediensList ){
        rec_id_generator++;
        recipeID = rec_id_generator;
        //date
        this.name = name;
        this.productType = productType;
        this.ingrediensList = ingrediensList;
    }

    public SimpleDateFormat getDate() {
        return date;
    }

    public List<Ingrediens> getIngrediensList() {
        return ingrediensList;
    }

    public String getName() {
        return name;
    }

    public String getProductType() {
        return productType;
    }

    public int getRecipeID() {
        return recipeID;
    }
}



