package system;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Controller {

    private List<Ingrediens> inventoryList = new ArrayList<>();
    private List<Recipe> recipeList = new ArrayList<>();
    
    public void registerNewIng(String name, int inventoryGrams) {
        //Feilhaandtering
        Ingrediens ingrediens = new Ingrediens(name, inventoryGrams);
        inventoryList.add(ingrediens);
    }

    public void registerNewRec(String name, String productType, List<Ingrediens> ingrediensList) {
        //Feilhaandtering
        Recipe recipe = new Recipe(name, productType, ingrediensList);
        recipeList.add(recipe);  
    }

    public void updateInventory(int ingID, int gramsAdded) {
        inventoryList.get(ingID-1).addGrams(gramsAdded);
    }

    public List<Recipe> getAllRecByType(String type) {
        List<Recipe> recipeListByType = new ArrayList<>();
        for (Recipe recipe : recipeList) {
            if (recipe.getProductType().equalsIgnoreCase(type)) {
                recipeListByType.add(recipe);
            }
        }
        recipeListByType.sort(new RecipeSorter());
        return recipeListByType;
    }

    public class RecipeSorter implements Comparator<Recipe> {

        @Override
        public int compare(Recipe o1, Recipe o2) {
            String a = o2.getName();
            String b = o1.getName();
            return a.compareTo(b);
        }
    }

    /*
    public boolean produceNumberOfProducts(int rec_id) {
        //Rakk ikke
    }

    public boolean removeAmountOfIng(int amount, int rec_id){
        //Rakk ikke
    }
    */
}
